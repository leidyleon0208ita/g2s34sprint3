
from Conexion_db import Conexion_db
class Crud:
    #la clase encargada de contener las queries 
    # y ejecutarlas a traves de la clase conexion_db

    def __init__(self,mi_host,database,user,passwd):
        self.conn = Conexion_db(mi_host,database,user,passwd)#creo el objeto conn como atributo
        #el objeto conn es un objeto de la clase Conexion_db y un atributo de la clase
        #Crud
    
    #se definen los metodos para escribir, leer, actualizar tablas 

    #para tabla clientes .... 
    def insertar_cliente(self,nombre, apellido, direccion, telefono, estrato, ):
        query = "INSERT INTO \"Clientes\""+\
        "(nombre, apellido, direccion, telefono, estrato)"+\
            "VALUES ('"+nombre+"','"+apellido+\
                "','"+direccion+"','"+telefono+\
                "','"+estrato+"')"
    
        self.conn.escribir_db(query)

    def leer_clientes(self):
        query= "SELECT * from \"Clientes\" ORDER BY id ASC"
        clientes = self.conn.consultar_db(query)
        return clientes

    def leer_cliente(self,id):
        query= "SELECT * from \"Clientes\" WHERE id="+id
        cliente = self.conn.consultar_db(query)
        return cliente
    
    def eliminar_cliente(self,id):
        query = "DELETE FROM \"Clientes\""+\
            "WHERE id="+str(id)
        self.conn.escribir_db(query)

    def eliminar_lectura(self,id):
        query = "DELETE FROM \"Lecturas\""+\
            "WHERE id="+str(id)
        self.conn.escribir_db(query)

    def leer_lecturas(self):
        query= "SELECT * from \"Lecturas\" ORDER BY id ASC"
        lecturas = self.conn.consultar_db(query)
        return lecturas

    def leer_lectura(self,id):
        query= "SELECT * from \"Lecturas\" WHERE id="+id
        lectura = self.conn.consultar_db(query)
        return lectura
        
    def leer_lectura_id_cliente(self,id_cliente):
        query= "SELECT * from \"Lecturas\" WHERE id_cliente="+id_cliente
        lectura = self.conn.consultar_db(query)
        return lectura

    def close(self):
        self.conn.cerrar_db()
    """
    def actualizar_cliente...
    ...
    ..
    #para la tabla lecturas
    def insertar_lectura(self, ):
        ...
        ..
    """
    def update_cliente(self,id,nombre,apellido,direccion,telefono,estrato):
        query= "UPDATE \"Clientes\"" +\
            " SET nombre='"+nombre+"', apellido='"+apellido +\
                "',direccion='"+direccion+"', telefono='"+telefono+\
                "',estrato="+estrato+\
                    " WHERE id="+id+";" 
        print(query)
        self.conn.escribir_db(query)

    def update_lectura(self,id,fecha,lectura,id_cliente):
        query= "UPDATE \"Lecturas\"" +\
            " SET fecha='"+fecha+"', lectura="+lectura +\
                ",id_cliente="+id_cliente+\
                " WHERE id="+id+";" 
        print(query)
        self.conn.escribir_db(query)
        

    def insertar_lectura(self,fecha, lectura, id_cliente, ):
        query = "INSERT INTO \"Lecturas\""+\
        "(fecha, lectura, id_cliente)"+\
            "VALUES ('"+fecha+"',"+lectura+\
                ","+id_cliente+")"
    
        self.conn.escribir_db(query)
