from os import stat
from server import database

class Lectura(database.Model):
    __tablename__='Lecturas'

    id = database.Column(database.Integer,primary_key=True)
    fecha = database.Column(database.Date, nullable=False)
    lectura = database.Column(database.Integer, nullable=False)
    id_cliente = database.Column(database.Integer, nullable=False)
    

    def __init__(self, fecha,lectura,id_cliente):
        self.fecha=fecha
        self.lectura=lectura
        self.id_cliente=id_cliente

    def __str__(self):
        return "<Lectura "+str(self.id)+": "+str(self.lectura)+">"

    def __repr__(self):
        return str(self)

    def create(self):
        database.session.add(self)
        database.session.commit()

    @staticmethod
    def delete(id_to_delete):
        Lectura.query.filter_by(id=id_to_delete).delete()
        database.session.commit()

    @staticmethod 
    def update_fech(id_to_update,new_fech):
        lectura = Lectura.query.get(id_to_update)
        lectura.modelo = new_fech
        database.session.commit()
        
    @staticmethod
    def get_all():
        return Lectura.query.order_by(Lectura.id.asc()).all()

    @staticmethod
    def get_id_client(id_client):
        return Lectura.query.filter_by(id_cliente=id_client).order_by(Lectura.id.asc()).all()

    @staticmethod
    def get_id_lectur(id_lectur):
        return Lectura.query.filter_by(id=id_lectur).first() #one()


""" 
class Viajes(database.Model):
    __tablename__ = "Viajes"
    id = database.Column(database.Integer,primary_key=True)
    id_pasajero = database.Column(database.Integer,database.ForeignKey('Pasajeros.id'))
    id_trayecto = database.Column(database.Integer,database.ForeignKey('Trayectos.id'))
    inicio = database.Column(database.String)
    fin =  database.Column(database.String)
    id_pasajero = database.relationship('Pasajeros',foreign_keys='Pasajeros.id')
    id_trayecto = database.relationship('Trayectos',foreign_keys='Trayectos.id')

    @staticmethod
    def example_join(id_pasajero):
        return Viajes.query.join(pasajeros,id_pasajero=pasajeros.id)
"""