from flask import Flask, request,make_response,redirect,render_template
from flask import Flask, request,make_response,redirect
from flask.wrappers import Response
from flask_sqlalchemy import SQLAlchemy
from Crud import Crud
import json 
import os

app = Flask(__name__) #instancia de flask

#postgresql://<nombre_usuario>:<password>@<host>:<puerto>/<nombre_basededatos>
app.config['SQLALCHEMY_DATABASE_URI']= 'postgresql://nyutjtlketbpff:723409cc2d1c8581b33555b9ba4ef4d58e3cffb11afd5935217d36ba59d57846@ec2-107-22-245-82.compute-1.amazonaws.com:5432/d44mad7hrjhaad'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = os.urandom(32)
database =SQLAlchemy(app)

from models.lectura import * 

"""
#forma de crear rutas 
@app.route('/')#ruta raiz 
#lo que se va a hacer cuando se acceda a esa ruta
#lo que se va a hacer se define a través de una funcion
def hello():  
    ip = request.remote_addr
    retorno = "su ip es: "+str(ip)  
    return retorno #esto sera mostrado en el navegador
"""
@app.route('/')
def index():
    '''user_ip = request.remote_addr
    response= make_response(redirect('/get_cliente'))
    response.set_cookie('user_ip',user_ip)'''
    return render_template("index.html")

    

@app.route('/hello')
def hello():
    user_ip=request.cookies.get('user_ip')
    return "la ip del PC es: "+str(user_ip)

'''@app.route('/division')
def division():
    div = 10/5
    return "la division es: "+str(div)

@app.route('/multiplicar')
def multiplicar():
    mult = 10*5    
    return "la multiplicacion es-+ : "+str(mult)

@app.route('/ejercicio')
def ejercicio():
    numero = int(input("ingrese un numero: "))
    return "el cuadrado del numero-es: "+str(numero**2)'''

@app.route('/get_cliente')
def get_cliente():
    cr = Crud("ec2-107-22-245-82.compute-1.amazonaws.com",
    "d44mad7hrjhaad",
    "nyutjtlketbpff","723409cc2d1c8581b33555b9ba4ef4d58e3cffb11afd5935217d36ba59d57846")
    clientes = cr.leer_clientes() #clientes es una lista de tuplas, cada registro es una tupla
    primer_cliente = clientes[0] #tomo el primer registro (la primera tupla)
    print(primer_cliente)
    respuesta = json.dumps( {"id": primer_cliente[0],
     "nombre": primer_cliente[1], "apellido":primer_cliente[2],
     "direccion": primer_cliente[3], "telefono":primer_cliente[4],
      "estrato":primer_cliente[5]})
    cr.close()#cerrando la conexion 
    """
    primer_bus_string ="id: "+str(primer_bus[0])+" modelo: "+str(primer_bus[1]) +\
        "capacidad: "+str(primer_bus[2])+\
            " placa: "+primer_bus[3] + "fabricante: "+primer_bus[4]
    """
    return respuesta


@app.route("/get_clientes")
def get_clientes():
    cr = Crud("ec2-107-22-245-82.compute-1.amazonaws.com",
    "d44mad7hrjhaad",
    "nyutjtlketbpff","723409cc2d1c8581b33555b9ba4ef4d58e3cffb11afd5935217d36ba59d57846")
    clientes = cr.leer_clientes() #buses es una lista de tuplas, cada registro es una tupla
    lista_clientes=[]
    for cliente in clientes:#cliente es la tupla de cada registro 
        lista_clientes.append({"id": cliente[0],
     "nombre": cliente[1], "apellido":cliente[2],
     "direccion": cliente[3], "telefono":cliente[4], 
     "estrato":cliente[5]})
    respuesta = json.dumps(lista_clientes)
    return render_template("mostrar_clientes.html",clientes=lista_clientes)

@app.route("/show_edit_cliente_form",methods=['GET','POST'])
def show_edit_cliente_form():
    
    if(request.method=='POST'):
        id_cliente=request.form.get("id_cliente")
        print("el id del cliente es: ",id_cliente)
        cr = Crud("ec2-107-22-245-82.compute-1.amazonaws.com",
        "d44mad7hrjhaad",
        "nyutjtlketbpff","723409cc2d1c8581b33555b9ba4ef4d58e3cffb11afd5935217d36ba59d57846")
        cliente = cr.leer_cliente(id_cliente) #buses es una lista de una tupla
        cr.close()
        cliente = cliente[0]
        print(cliente)
        dic_cliente = {"id":cliente[0],"nombre":cliente[1],"apellido":cliente[2],"direccion":cliente[3],"telefono":cliente[4],"estrato":cliente[5]}
        return render_template("edit_cliente.html",cliente=dic_cliente)

@app.route("/edit_cliente",methods=['GET','POST'])
def edit_cliente():
    if(request.method=='POST'):
        id=request.form.get("id")
        nombre=request.form.get("nombre")
        apellido=request.form.get("apellido")
        direccion=request.form.get("direccion")
        telefono=request.form.get("telefono")
        estrato=request.form.get("estrato")
        print("id=",id," nombre=",nombre, " apellido=",apellido, " direccion=",direccion," telefono=",telefono, " estrato=",estrato)
        cr = Crud("ec2-107-22-245-82.compute-1.amazonaws.com",
        "d44mad7hrjhaad",
        "nyutjtlketbpff","723409cc2d1c8581b33555b9ba4ef4d58e3cffb11afd5935217d36ba59d57846")
        cr.update_cliente(id,nombre,apellido,direccion,telefono,estrato)
        cr.close()
        response= make_response(redirect('/get_clientes'))
        return response

@app.route("/delete_cliente",methods=['GET','POST'])
def delete_cliente():
    if(request.method=='POST'):
        id_cliente=request.form.get("id_cliente")
        print("el id del cliente es=",id_cliente)
        cr = Crud("ec2-107-22-245-82.compute-1.amazonaws.com",
        "d44mad7hrjhaad",
        "nyutjtlketbpff","723409cc2d1c8581b33555b9ba4ef4d58e3cffb11afd5935217d36ba59d57846")
        cr.eliminar_cliente(id_cliente)
        cr.close()
        response= make_response(redirect('/get_clientes'))
        return response


'''
Mostar las lecturas utilizando el CRUD
@app.route("/get_lecturas")
def get_lecturas():
    cr = Crud("ec2-107-22-245-82.compute-1.amazonaws.com",
    "d44mad7hrjhaad",
    "nyutjtlketbpff","723409cc2d1c8581b33555b9ba4ef4d58e3cffb11afd5935217d36ba59d57846")
    lecturas = cr.leer_lecturas() #buses es una lista de tuplas, cada registro es una tupla
    lista_lecturas=[]
    for lectura in lecturas:#cliente es la tupla de cada registro 
        lista_lecturas.append({"id": lectura[0],
     "fecha": lectura[1], "lectura":lectura[2],
     "id_cliente": lectura[3]})
    respuesta = json.dumps(lista_lecturas)#Comentarlo 
    return render_template("mostrar_lecturas.html",lecturas=lista_lecturas)'''

@app.route("/get_lecturas")
def get_lecturas():
    
    return render_template("mostrar_lecturas.html",lecturas=Lectura.get_all())
        
'''@app.route("/show_edit_lectura_form",methods=['GET','POST'])
def show_edit_lectura_form():
    if(request.method=='POST'):
        id_lectura=request.form.get("id_lectura")
        print("el id de la lectura es: ",id_lectura)
        cr = Crud("ec2-107-22-245-82.compute-1.amazonaws.com",
        "d44mad7hrjhaad",
        "nyutjtlketbpff","723409cc2d1c8581b33555b9ba4ef4d58e3cffb11afd5935217d36ba59d57846")
        lectura = cr.leer_lectura(id_lectura) #buses es una lista de una tupla
        cr.close()
        lectura = lectura[0]
        print(lectura)
        dic_lectura = {"id":lectura[0],"fecha":lectura[1],"lectura":lectura[2],"id_cliente":lectura[3]}
        return render_template("edit_lectura.html",lectura=dic_lectura)'''

@app.route("/show_edit_lectura_form",methods=['GET','POST'])
def show_edit_lectura_form():
    if(request.method=='POST'):
        id_lectura=request.form.get("id_lectura")
        print("el id de la lectura es: ",id_lectura)
        lectura=Lectura.get_id_lectur(id_lectura)
        print("la lectura es :", type(lectura))
        return render_template("edit_lectura.html",lectura=lectura)

@app.route("/edit_lectura",methods=['GET','POST'])
def edit_lectura():
    if(request.method=='POST'):
        id=request.form.get("id")
        fecha=request.form.get("fecha")
        lectura=request.form.get("lectura")
        id_cliente=request.form.get("id_cliente")
        print("id=",id," fecha=",fecha, " lectura=",lectura, " id_cliente=",id_cliente)
        cr = Crud("ec2-107-22-245-82.compute-1.amazonaws.com",
        "d44mad7hrjhaad",
        "nyutjtlketbpff","723409cc2d1c8581b33555b9ba4ef4d58e3cffb11afd5935217d36ba59d57846")
        cr.update_lectura(id,fecha,lectura,id_cliente)
        cr.close()
        response= make_response(redirect('/get_lecturas'))
        return response

'''
Este es el método para eliminar usando el CRUD
@app.route("/delete_lectura",methods=['GET','POST'])
def delete_lectura():
    if(request.method=='POST'):
        id_lectura=request.form.get("id_lectura")
        print("el id de la lectura es=",id_lectura)
        cr = Crud("ec2-107-22-245-82.compute-1.amazonaws.com",
        "d44mad7hrjhaad",
        "nyutjtlketbpff","723409cc2d1c8581b33555b9ba4ef4d58e3cffb11afd5935217d36ba59d57846")
        cr.eliminar_lectura(id_lectura)
        cr.close()
        response= make_response(redirect('/get_lecturas'))
        return response'''

'''Este es el método para eliminar una lectura utilizando alchemy'''
@app.route("/delete_lectura",methods=['GET','POST'])
def delete_lectura():
    if(request.method=='POST'):
        id_lectura=request.form.get("id_lectura")
        print("el id de la lectura es=",id_lectura)
        response= make_response(redirect('/get_lecturas'))
        Lectura.delete(id_lectura)
        return response

@app.route("/show_add_lectura_form",methods=['GET','POST'])
def show_add_lectura_form():
    if(request.method=='GET'):
        
        return render_template("insert_lectura.html")

@app.route("/insertar_lectura",methods=['GET','POST'])
def insertar_lectura():
    if(request.method=='POST'):
        fecha=request.form.get("fecha")
        lectura=request.form.get("lectura")
        id_cliente=request.form.get("id_cliente")
        print(" fecha=",fecha, " lectura=",lectura, " id_cliente=",id_cliente)
        cr = Crud("ec2-107-22-245-82.compute-1.amazonaws.com",
        "d44mad7hrjhaad",
        "nyutjtlketbpff","723409cc2d1c8581b33555b9ba4ef4d58e3cffb11afd5935217d36ba59d57846")
        cr.insertar_lectura(fecha,lectura,id_cliente)
        cr.close()
        response= make_response(redirect('/get_lecturas'))
        return response

@app.route("/show_consultar_cliente_form",methods=['GET','POST'])
def show_consultar_cliente_form():
    if(request.method=='GET'):
        
        return render_template("consultar_cliente.html")


@app.route("/consultar_cliente",methods=['GET','POST'])
def consultar_cliente():
    if(request.method=='POST'):
        id_cliente=request.form.get("id_cliente")
        print("id del cliente es=",id_cliente)
    elif (request.method=='GET'):
        rdata=request.data
        print(rdata)
        id_cliente=str(rdata["cli"])
    '''cr = Crud("ec2-107-22-245-82.compute-1.amazonaws.com",
    "d44mad7hrjhaad",
    "nyutjtlketbpff","723409cc2d1c8581b33555b9ba4ef4d58e3cffb11afd5935217d36ba59d57846")
    lecturas=cr.leer_lectura(id_cliente)
    lista_lecturas=[]
    for lectura in lecturas:#cliente es la tupla de cada registro 
        lista_lecturas.append({"id": lectura[0],
        "fecha": lectura[1], "lectura":lectura[2],
        "id_cliente": lectura[3]})
    cr.close()'''
    lecturas=Lectura.get_id_client(id_cliente)
    if len(lecturas)>1:
        consumo=abs(lecturas[len(lecturas)-1].lectura-lecturas[len(lecturas)-2].lectura)
    elif len(lecturas)==1:
        consumo=lecturas[0].lectura
    else:
        consumo=0
    huella_carbono=consumo*2.860*1000
    #database.session.add(Lectura(fecha="2021-09-09",lectura=770,id_cliente=10))# .save()
    #database.session.commit()
    return render_template("mostrar_lecturas_id_cliente.html",lecturas=lecturas, consumo=consumo,huella_carbono=huella_carbono)












if __name__=="__main__":
    #punto de partida de ejecucion del programa    
    print("arrancando servidor...")
    app.run(debug=True,host="0.0.0.0")
